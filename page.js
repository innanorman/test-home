var accItem = document.getElementsByClassName('faq-item-list');
var accHD = document.getElementsByClassName('faq-item-list-head');
for (i = 0; i < accHD.length; i++) {
    accHD[i].addEventListener('click', toggleItem, false);
}
function toggleItem() {
    var itemClass = this.parentNode.className;
    for (i = 0; i < accItem.length; i++) {
        accItem[i].className = 'faq-item-list close';
    }
    if (itemClass == 'faq-item-list close') {
        this.parentNode.className = 'faq-item-list open';
    }
}